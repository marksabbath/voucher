# Vouchers API

This is a simple API that simulates a voucher pool.

## How to install this API

1. Make sure you have PHP 7.1.2 or later, and install Lumen and its tools in your global system running `composer global require "laravel/lumen-installer"`;
2. After cloning this repository, go to `voucher-api/` directory and run `composer install`;
3. The Next step would be configuring the database. Edit the file `voucher-api/.env` and add the database credentials. i.e.:
    ```
    APP_ENV=local
    APP_DEBUG=true
    APP_KEY=
    APP_TIMEZONE=UTC

    LOG_CHANNEL=stack
    LOG_SLACK_WEBHOOK_URL=

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=<database_name>
    DB_USERNAME=<database_username>
    DB_PASSWORD=<database_password>

    CACHE_DRIVER=file
    QUEUE_DRIVER=sync
    ```
4. With the database working, run `php artisan migrate:install`. It will create the database;
4. After that, run `php artisan migrate`. It will create all the necessary tables;
5. The last step would be changing to `voucher-api` directory, and run: `php -S localhost:<port> -t public`.

By following these steps, you should be able to start using the API.

## Endpoints

The endpoints available are:

`/api/customers`

`/api/offers`

`/api/vouchers`

### Rules implemented
 - For each *customer*, the system will generate vouchers for each *offer* not expired;
 - When deleting a *customer*, the system will clean the *vouchers* associated with that *customer*;
 - It's not possible to remove *vouchers* associated with a *customer*.

## Customer Endpoints

### Insert a new customer

```
curl -X POST http://<api_address>/api/customers -H "Content-Type: application/json" -d '{"name": <customer_name>, "email": <customer_email>}'
```

### Retrieve all customers

```
curl http://<api_address>/api/customers
```

### Retrieve one customer

```
curl http://<api_address>/api/customers/<customer_id>
```

### Delete a customer

```
curl -X DELETE http://<api_address>/api/customers/<customer_id>
```

## Offer Endpoints

### Insert a new offer

```
curl -X POST http://<api_address>/api/offers -H "Content-Type: application/json" -d '{"name": <offer_name>, "discount": <discount>, "expires_at": <expiration_date>}'
```

### Retrieve all offer

```
curl http://<api_address>/api/offers
```

### Retrieve one offer

```
curl http://<api_address>/api/offers/<offer_id>
```

### Delete a offer

```
curl -X DELETE http://<api_address>/api/offers/<offer_id>
```

## Vouchers Endpoints

### Insert a new voucher

```
curl -X POST http://<api_address>/api/vouchers -H "Content-Type: application/json" -d '{"recipient_id": <recipient_id>, "offer_id": <offer_id>, "voucher_code": <voucher_code>}'
```
**It needs to have a user and offer previously available in database, otherwise, it will not allow to add a voucher.**

### Retrieve all vouchers

```
curl http://<api_address>/api/vouchers
```

### Retrieve one voucher

```
curl http://<api_address>/api/vouchers/<voucher_id>
```

### Retrieve all vouchers given an Customer email

```
curl http://<api_address>/api/vouchers/?email=<customer_email>
```

### Delete a voucher

```
curl -X DELETE http://<api_address>/api/vouchers/<voucher_id>
```

### Validate a voucher

```
curl -X DELETE http://<api_address>/api/vouchers/validate -H "Content-Type: application/json" -d '{"email": <customer_email>,  "voucher_code": <voucher_code>}'
```

## Tests

Inside the directory `tests` you will find the test files. These tests covers mostly all the API calls. To run these tests, you just need to run `vendor/bin/phpunit`.
