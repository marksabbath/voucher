<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
  $router->get('customers',  ['uses' => 'CustomerController@showAllCustomers']);

  $router->get('customers/{id}', ['uses' => 'CustomerController@showOneCustomer']);

  $router->post('customers', ['uses' => 'CustomerController@create']);

  $router->delete('customers/{id}', ['uses' => 'CustomerController@delete']);

  $router->put('customers/{id}', ['uses' => 'CustomerController@update']);

 //----
  $router->get('offers',  ['uses' => 'OfferController@showAllOffers']);

  $router->get('offers/{id}', ['uses' => 'OfferController@showOneOffer']);

  $router->post('offers', ['uses' => 'OfferController@create']);

  $router->delete('offers/{id}', ['uses' => 'OfferController@delete']);

  $router->put('offers/{id}', ['uses' => 'OfferController@update']);

  //----
  $router->get('vouchers',  ['uses' => 'VoucherController@showAllVouchers']);

  $router->get('vouchers/{id}', ['uses' => 'VoucherController@showOneVoucher']);

  $router->post('vouchers', ['uses' => 'VoucherController@create']);

  $router->delete('vouchers/{id}', ['uses' => 'VoucherController@delete']);

  $router->put('vouchers/{id}', ['uses' => 'VoucherController@update']);

  $router->post('vouchers/validate', ['uses' => 'VoucherController@validateVoucher']);


});
