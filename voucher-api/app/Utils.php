<?php

namespace App;

class Utils
{
    /**
     * Generates random tokens.
     *
     * @return string Token.
     */
    public static function generateToken()
    {
        return bin2hex(random_bytes(10));
    }
}
