<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Controllers\VoucherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{

    public function showAllCustomers()
    {
        return response()->json(Customer::all());
    }

    public function showOneCustomer($id)
    {
        return response()->json(Customer::find($id));
    }

    public function create(Request $request)
    {
        $customer = Customer::create($request->all());

        $this->generateToken($customer->id);

        return response()->json($customer, 201);
    }

    public function update($id, Request $request)
    {
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        return response()->json($customer, 200);
    }

    public function delete($id)
    {
        VoucherController::removeVouchersFromCustomer($id);

        Customer::findOrFail($id)->delete();

        return response('Deleted Successfully', 200);
    }

    /**
     * Verifies if a customer is valid.
     *
     * @param  int  $id Customer id.
     * @return boolean  True if customer is valid, otherwise false.
     */
    public static function isCustomerValid($id)
    {
        $customer = DB::table('customers')
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->get();

        return (sizeof($customer) == 1) ? true : false;
    }

    /**
     * Generates unexpired vouchers for each new customer added.
     *
     * @param int $customer_id New customer Id.
     *
     * @since 0.0
     */
    public function generateToken($customer_id)
    {
        date_default_timezone_set("America/Chicago");
        $offers = DB::table('offers')
                    ->where('expires_at', '>', date("Y-m-d H:i:s"))
                    ->get();

        foreach($offers as $offer)
        {
            VoucherController::generateVoucher($customer_id, $offer->id);
        }
    }
}
