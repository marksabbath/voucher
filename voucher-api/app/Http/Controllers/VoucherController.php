<?php

namespace App\Http\Controllers;

use App\Voucher;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoucherController extends Controller
{

    public function showAllVouchers(Request $request)
    {
        $email = $request->query('email');
        if(!is_null($email))
        {
            return response()->json($this->showCustomerValidVouchers($email));
        }

        return response()->json(Voucher::all());
    }

    public function showOneVoucher($id)
    {
        return response()->json(Voucher::find($id));
    }

    public function create(Request $request)
    {

        if($this->isAllowedToAdd($request->offer_id, $request->recipient_id))
        {
            $voucher = Voucher::create($request->all());
            return response()->json($voucher, 201);
        }

        return response('Could not add the voucher.', 500);
    }

    public function update($id, Request $request)
    {
        $voucher = Voucher::findOrFail($id);
        $voucher->update($request->all());

        return response()->json($voucher, 200);
    }

    public function delete($id)
    {
        Voucher::findOrFail($id)->delete();
        return response('Deleted Successfully.', 200);
    }

    /**
     * Generate token given a email and offer id.
     *
     * @param int $recipient_id Email id.
     * @param int $offer_id Offer id.
     *
     * @return void
     *
     * @since 0.0
     */
    public static function generateVoucher($recipient_id, $offer_id)
    {
        $data = array(
            'recipient_id' => $recipient_id,
            'offer_id' => $offer_id,
            'voucher_code' => Utils::generateToken()
        );

        Voucher::create($data);
    }

    /**
     * Validate/use voucher
     *
     * @param  Request $request Request data.
     *
     * @return Response|JSON Response.
     */
    public function validateVoucher(Request $request)
    {
        $query = DB::table('vouchers')
                        ->join('customers', 'vouchers.recipient_id', '=', 'customers.id')
                        ->join('offers', 'vouchers.offer_id', '=', 'offers.id')
                        ->select('vouchers.id', 'offers.discount')
                        ->where([
                            ['customers.email', '=', $request->email],
                            ['vouchers.voucher_code', '=', $request->voucher_code],
                            ['vouchers.used_at', '=', null]
                        ])
                        ->get();

        if(sizeof($query) == 1)
        {
            date_default_timezone_set("America/Chicago");
            $voucher = Voucher::findOrFail($query[0]->id);
            $voucher->used_at = date("Y-m-d H:i:s");
            $voucher->update($voucher->getAttributes());

            $response = array(
                "discount" => $query[0]->discount
            );

            return response()->json($response, 200);
        }

        return response('Could not validate the voucher.', 200);
    }

    /**
     * Check if the voucher can be added.
     *
     * @param string  $offer_id Offer Id.
     * @param string  $recipient_id Customer Id.
     *
     * @return boolean True if allowed, otherwise false.
     *
     * @since 0.0
     */
    public static function isAllowedToAdd($offer_id, $recipient_id)
    {
        $voucher = DB::table('vouchers')
                    ->where([
                        ['recipient_id', '=', $recipient_id],
                        ['offer_id', '=', $offer_id]
                    ])
                    ->get();

        return (OfferController::isOfferAvailable($offer_id) && CustomerController::isCustomerValid($recipient_id)) ? true  : false;
    }

    /**
     * Show all non expired vouchers given an email.
     *
     * @param string $email Customer email address.
     *
     * @return array        Database respose.
     *
     * @since 1.1
     */
    public static function showCustomerValidVouchers($email)
    {
        date_default_timezone_set("America/Chicago");
        $vouchers = DB::table('vouchers')
                        ->join('customers', 'vouchers.recipient_id', '=', 'customers.id')
                        ->join('offers', 'vouchers.offer_id', '=', 'offers.id')
                        ->select('vouchers.voucher_code', 'offers.name')
                        ->where([
                            ['customers.email', '=', $email],
                            ['vouchers.used_at', '=', null],
                            ['offers.expires_at', '>', date("Y-m-d H:i:s")]
                        ])
                        ->get();

        return $vouchers;
    }

    /**
     * Remove vouchers associated with an Customer Id.
     *
     * @param  int $recipient_id Recipiend/Customer id.
     *
     * @return void
     *
     * @since 0.0
     */
    public static function removeVouchersFromCustomer($recipient_id)
    {
        $vouchers = DB::table('vouchers')
                    ->select('vouchers.id')
                    ->where('recipient_id', '=', $recipient_id)
                    ->get();

        foreach($vouchers as $voucher)
        {
            Voucher::findOrFail($voucher->id)->delete();
        }
    }
}
