<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Voucher;
use App\Http\Controllers\VoucherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfferController extends Controller
{

    /**
     * Response with all offers.
     *
     * @return json All offers available.
     *
     * @since 0.0
     */
    public function showAllOffers()
    {
        return response()->json(Offer::all());
    }

    public function showOneOffer($id)
    {
        return response()->json(Offer::find($id));
    }

    public function create(Request $request)
    {
        $offer = Offer::create($request->all());

        // This is here only to make this simple system work without adding
        // a lot of O.S. settings. My approach in these cases would be creating
        // a new file that would be triggered by a O.S. cron job, like */5 mins
        // checking if a new offer is available.
        $this->generateTokens($offer['id']);

        return response()->json($offer, 201);
    }

    public function update($id, Request $request)
    {
        $offer = Offer::findOrFail($id);
        $offer->update($request->all());

        return response()->json($offer, 200);
    }

    public function delete($id)
    {
        Offer::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    /**
     * Check if a given offer is not expired.
     * @param string $id Offer Id.
     *
     * @return boolean True if not expired, otherwise false.
     *
     * @since 0.0
     */
    public static function isOfferAvailable($id)
    {
        date_default_timezone_set( "America/Chicago" );
        $offer = DB::table('offers')
                ->where([
                    ['id', '=', $id],
                    ['expires_at', '>', date( "Y-m-d H:i:s" )]
                ])
                ->get();

        return (sizeof($offer) > 0) ? true : false;
    }

    /**
     * Get all non expired offers.
     *
     * @return array Offers non expired.
     */
    public function getAvailableOffers()
    {
        date_default_timezone_set( "America/Chicago" );
        $offers = DB::table('offers')
                    ->where('expires_at', '>', date( "Y-m-d H:i:s" ))
                    ->get();

        return $offers;
    }

    /**
     * Generate tokens for each new offer.
     *
     * @param int $offer_id New offer Id.
     *
     * @since 0.0
     */
    public function generateTokens($offer_id)
    {
        $customers = DB::table('customers')->get();

        foreach($customers as $customer)
        {
            VoucherController::generateVoucher($customer->id, $offer_id);
        }
    }
}
