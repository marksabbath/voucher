<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

abstract class ApiTestCase extends TestCase
{
    /**
     * URI to use in the calls.
     *
     * @var string
     */
    public $uri = '';

    /**
     * Data to be added using the POST method.
     *
     * @var array
     */
    public $data = array();

    /**
     * The JSON structure.
     *
     * @var array
     */
    public $jsonStructure = array();

    /**
     * Id of the new customer added into the API.
     *
     * @var int
     */
    public $insertedDataResponse = null;

    /**
     * Test the Customer POST API.
     *
     * @return void
     *
     * @since 0.0
     */

    public function testInsertNewData()
    {
        $response = $this->call('POST', $this->uri, $this->data);

        $this->insertedDataResponse = json_decode($response->getContent(), true);

        print_r($this->insertedDataResponse['id']);

        $this->assertEquals(201, $response->status());
    }

    /**
     * Test the Custoemr GET API.
     *
     * @return void
     *
     * @since 0.0
     */
    public function testGetData()
    {
        $response = $this->call('GET', $this->uri);

        $this->assertEquals(200, $response->status());

        $this->seeJsonStructure($this->jsonStructure);
    }

    /**
     * Test the Customer DELETE API.
     *
     * @return void
     *
     * @since 0.0
     */
    // public function testDeleteData()
    // {
    //     $response = $this->call('DELETE', $this->uri . '/' . $this->insertedDataResponse['id']);
    //
    //     $this->assertEquals(200, $response->status());
    // }
}
