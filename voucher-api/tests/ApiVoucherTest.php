<?php

require_once('ApiTestCase.php');

class ApiVoucherTest extends ApiTestCase
{

    public $uri = '/api/vouchers';

    public $data = array(
        'voucher_code' => 'AAAABBBCCCDDDEEFF',
    );

    public $insertedDataResponse = array();

    public $jsonStructure = array(
        [
            'id',
            'recipient_id',
            'voucher_code',
            'offer_id',
            'used_at',
            'created_at',
            'updated_at',
        ]
    );

    public function testInsertNewData()
    {
        $customer_id = 0;
        $offer_id = 0;

        $customers = $this->call('GET', '/api/customers');
        $customers = json_decode($customers->getContent(), true);

        foreach($customers as $customer)
        {
            if($customer['name'] === 'Test Api')
            {
                $customer_id = $customer['id'];
            }
        }

        $offers = $this->call('GET', '/api/offers');
        $offers = json_decode($offers->getContent(), true);

        foreach($offers as $offer)
        {
            if($offer['name'] === 'Offer Unit Test')
            {
                $offer_id = $offer['id'];
            }
        }

        $this->data['recipient_id'] = $customer_id;
        $this->data['offer_id'] = $offer_id;

        $response = $this->call('POST', $this->uri, $this->data);

        print_r($this->data);

        $this->assertEquals(201, $response->status());
    }
}
