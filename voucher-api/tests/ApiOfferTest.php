<?php

require_once('ApiTestCase.php');

class ApiOfferTest extends ApiTestCase
{

    public $uri = '/api/offers';

    public $data = array(
        'name' => 'Offer Unit Test',
        'discount' => 20,
        'expires_at' => '2018-12-31',
    );

    public $insertedDataResponse = array();

    public $jsonStructure = array(
        [
            'id',
            'name',
            'discount',
            'expires_at',
            'created_at',
            'updated_at',
        ]
    );
}
