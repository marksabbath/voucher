<?php

require_once('ApiTestCase.php');

class ApiCustomerTest extends ApiTestCase
{

    public $uri = '/api/customers';

    public $data = array(
        'name' => 'Test Api',
        'email' => 'testapi@test.com',
    );

    public $insertedDataResponse = array();

    public $jsonStructure = array(
        [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at',
        ]
    );
}
